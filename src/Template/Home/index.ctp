<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Painel de Controle
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?=$conluidas?></h3>

                    <p>Tarefas Conluidas</p>
                </div>
                <div class="icon">
                    <i class="fa fa-archive"></i>
                </div>
                <?= $this->Html->link(__('Mais Informações ').'<i class="fa fa-arrow-circle-right"></i>',
                    ['controller'=>'Tarefas', 'action'=>'index'],
                    ['escape'=> false, 'class'=>'small-box-footer'])?>


            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?=$abertas?></h3>

                    <p>Tarefas em Aberto</p>
                </div>
                <div class="icon">
                    <i class="fa fa-book"></i>
                </div>
                <?= $this->Html->link(__('Mais Informações ').'<i class="fa fa-arrow-circle-right"></i>',
                    ['controller'=>'Tarefas', 'action'=>'index'],
                    ['escape'=> false, 'class'=>'small-box-footer'])?>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?=$atrasadas?></h3>

                    <p>Tarefas em Atraso</p>
                </div>
                <div class="icon">
                    <i class="fa fa-warning "></i>
                </div>
                <?= $this->Html->link(__('Mais Informações ').'<i class="fa fa-arrow-circle-right"></i>',
                    ['controller'=>'Tarefas', 'action'=>'index'],
                    ['escape'=> false, 'class'=>'small-box-footer'])?>
            </div>
        </div>
    </div>
</section>
