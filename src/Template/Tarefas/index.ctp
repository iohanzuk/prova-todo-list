<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Tarefas
        <div class="pull-right"><?= $this->Html->link(__('Novo'), ['action' => 'add'],
                ['class' => 'btn btn-success btn-xs']) ?></div>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <?= $this->Form->create('Tarefas', ['method' => 'post']) ?>
                <div class="box-header with-border">
                    <h3 class="box-title">Filtros</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-3">
                        <?= $this->Form->input('nome', ['label' => 'Nome']); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('categoria_id', ['label' => 'Categoria',
                            'data-fill' => 'send', 'name' => 'categorias', 'type' => 'select',
                            'empty' => 'Selecione', 'options' => $categorias]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('situacao', ['label' => 'Situação da Tarefa', 'name' => 'situacao',
                            'empty' => 'Selecione', 'options' => [1 => 'Em Aberto', 2 => 'Em Atraso',
                                3 => 'Concluida']]); ?>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <?= $this->Form->button('<i class="fa fa-search"></i>', ['type' => 'submit', 'id' => 'btn-fill',
                        'class' => 'btn btn-default', 'data-placement' => 'bottom', 'title' => 'Pesquisar',
                        'escape' => false]) ?>
                    <?= $this->Form->button('<i class="fa fa-refresh"></i>', ['type' => 'button', 'id' => 'btn-refresh',
                        'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom',
                        'title' => 'Limpar Filtros', 'escape' => false]) ?>

                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= __('List of') ?> Tarefas</h3>
                    <div class="box-tools">
                        <form action="<?php echo $this->Url->build(); ?>" method="POST">
                        </form>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('id') ?></th>
                            <th><?= $this->Paginator->sort('categoria_id') ?></th>
                            <th><?= $this->Paginator->sort('nome') ?></th>
                            <th><?= $this->Paginator->sort('data_inicio') ?></th>
                            <th><?= $this->Paginator->sort('data_fim') ?></th>
                            <th><?= __('Ações') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($tarefas as $tarefa): ?>
                            <tr>
                                <td><?= $this->Number->format($tarefa->id) ?></td>
                                <td><?= $tarefa->has('categoria') ? $this->Html->link($tarefa->categoria->nome, ['controller' => 'Categorias', 'action' => 'view', $tarefa->categoria->id]) : '' ?></td>
                                <td><?= h($tarefa->nome) ?></td>
                                <td><?= h($tarefa->data_inicio) ?></td>
                                <td><?= h($tarefa->data_fim) ?></td>
                                <td class="actions" style="white-space:nowrap">
                                    <?php if (!$tarefa->concluido): ?>
                                    <?= $this->Form->postLink(__('Concluir'), ['action' => 'concluir-tarefa', $tarefa->id], ['confirm' => __('Finalizar Tarefa ?'), 'class' => 'btn btn-success btn-xs']) ?>
                                    <?php endif; ?>
                                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $tarefa->id], ['class' => 'btn btn-info btn-xs']) ?>
                                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $tarefa->id], ['class' => 'btn btn-warning btn-xs']) ?>
                                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $tarefa->id], ['confirm' => __('Confirm to delete this entry?'), 'class' => 'btn btn-danger btn-xs']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php echo $this->Paginator->numbers(); ?>
                    </ul>
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
