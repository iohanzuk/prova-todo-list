<section class="content-header">
  <h1>
    <?php echo __('Tarefa'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                        <dt><?= __('Categoria') ?></dt>
                                <dd>
                                    <?= $tarefa->has('categoria') ? $tarefa->categoria->id : '' ?>
                                </dd>
                                                                                                                        <dt><?= __('Nome') ?></dt>
                                        <dd>
                                            <?= h($tarefa->nome) ?>
                                        </dd>
                                                                                                                                    
                                            
                                                                                                                                            
                                                                                                        <dt><?= __('Data Inicio') ?></dt>
                                <dd>
                                    <?= h($tarefa->data_inicio) ?>
                                </dd>
                                                                                                                    <dt><?= __('Data Fim') ?></dt>
                                <dd>
                                    <?= h($tarefa->data_fim) ?>
                                </dd>
                                                                                                                                                                                                            
                                            
                                                                        <dt><?= __('Descricao') ?></dt>
                            <dd>
                            <?= $this->Text->autoParagraph(h($tarefa->descricao)); ?>
                            </dd>
                                                            </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

</section>
