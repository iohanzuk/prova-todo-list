<?php
use Cake\Core\Configure;

$file = Configure::read('Theme.folder'). DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'aside' . DS . 'sidebar-menu.ctp';
if (file_exists($file)) {
    ob_start();
    include_once $file;
    echo ob_get_clean();
} else {
?>
<ul class="sidebar-menu">
    <li class="header">Menu</li>
    <li class="treeview">
        <?= $this->Html->link('<i class="fa fa-dashboard"></i> '.__('Painel Inicial'), ['controller'=>'Home',
            'action' => 'index'], ['escape' => false]) ?>
    </li>
    <li class="treeview">
        <?= $this->Html->link('<i class="fa fa-book"></i> '.__('Tarefas'), ['controller'=>'Tarefas',
            'action' => 'index'], ['escape' => false]) ?>
    </li>
    <li class="treeview">
        <?= $this->Html->link('<i class="fa fa-tasks"></i> '.__('Categorias'), ['controller'=>'Categorias',
            'action' => 'index'], ['escape' => false]) ?>
    </li>
</ul>
<?php } ?>
