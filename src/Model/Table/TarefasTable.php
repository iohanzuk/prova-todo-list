<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use DateTime;

/**
 * Tarefas Model
 *
 * @property \App\Model\Table\CategoriasTable|\Cake\ORM\Association\BelongsTo $Categorias
 *
 * @method \App\Model\Entity\Tarefa get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tarefa newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tarefa[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tarefa|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tarefa patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tarefa[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tarefa findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TarefasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tarefas');
        $this->setDisplayField('nome');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Categorias', [
            'foreignKey' => 'categoria_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->scalar('descricao')
            ->allowEmpty('descricao');

        $validator
            ->dateTime('data_inicio')
            ->allowEmpty('data_inicio');

        $validator
            ->dateTime('data_fim')
            ->allowEmpty('data_fim');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['categoria_id'], 'Categorias'));

        return $rules;
    }

    public function tarefasConcluidas(){
       return $this->find('all')->where(['Tarefas.concluido'=>true])->count();
    }

    public function tarefasAbertas(){
        $now = new DateTime();
        return $this->find('all')
            ->where(['Tarefas.concluido'=>false, 'Tarefas.data_inicio >' => $now])
            ->count();
    }

    public function tarefasAtrasadas(){
        $now = new DateTime();
        return $this->find('all')
            ->where(['Tarefas.concluido'=>false, 'Tarefas.data_inicio <=' => $now])
            ->count();
    }

    public function filtros($nome = null, $categoria = null, $situacao = null){
        $now = new DateTime();
        $tarefa = $this->find('all')->where();
        if(!empty($nome))
            $tarefa->andWhere(['Tarefas.nome LIKE "%'.$nome.'%"']);
        if(!empty($categoria))
            $tarefa->andWhere(['Tarefas.categoria_id'=>$categoria]);
        if(!empty($situacao)){
            switch ($situacao){
                case 1:
                    $tarefa->andWhere(['Tarefas.concluido'=>false, 'Tarefas.data_inicio >' => $now]);
                    break;
                case 2:
                    $tarefa->andWhere(['Tarefas.concluido'=>false, 'Tarefas.data_inicio <=' => $now]);
                    break;
                case 3:
                    $tarefa->andWhere(['Tarefas.concluido'=>true]);
                    break;
            }
        }
        debug($categoria);
        return $tarefa;
    }
}
