<?php
namespace App\Controller;

use App\Controller\AppController;


class HomeController extends AppController{
    public function index(){
        $this->loadModel('Tarefas');
        $conluidas = $this->Tarefas->tarefasConcluidas();
        $abertas = $this->Tarefas->tarefasAbertas();
        $atrasadas = $this->Tarefas->tarefasAtrasadas();

        $this->set(compact('conluidas','abertas','atrasadas'));
    }
}