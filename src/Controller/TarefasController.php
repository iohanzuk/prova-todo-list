<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Tarefas Controller
 *
 * @property \App\Model\Table\TarefasTable $Tarefas
 *
 * @method \App\Model\Entity\Tarefa[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TarefasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $this->loadModel('Categorias');
        $categorias = $this->Categorias->find('list');
        $this->paginate = [
            'contain' => ['Categorias']
        ];
        if ($this->request->is('post')) {
            $tarefas = $this->Tarefas->filtros(
                $this->request->getData('nome'),
                $this->request->getData('categorias'),
                $this->request->getData('situacao')
            );
            $tarefas = $this->paginate($tarefas);
        } else {

            $tarefas = $this->paginate($this->Tarefas);
        }
        $this->set(compact('tarefas', 'categorias'));
    }

    /**
     * View method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tarefa = $this->Tarefas->get($id, [
            'contain' => ['Categorias']
        ]);

        $this->set('tarefa', $tarefa);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tarefa = $this->Tarefas->newEntity();
        if ($this->request->is('post')) {
            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->data);
            if ($this->Tarefas->save($tarefa)) {
                $this->Flash->success(__('A {0} foi salva!', 'Tarefa'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('A {0} não pode ser salva', 'Tarefa'));
            }
        }
        $categorias = $this->Tarefas->Categorias->find('list', ['limit' => 200]);
        $this->set(compact('tarefa', 'categorias'));
        $this->set('_serialize', ['tarefa']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tarefa = $this->Tarefas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tarefa = $this->Tarefas->patchEntity($tarefa, $this->request->data);
            if ($this->Tarefas->save($tarefa)) {
                $this->Flash->success(__('A {0} foi salva!', 'Tarefa'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('A {0} não pode ser salva', 'Tarefa'));
            }
        }
        $categorias = $this->Tarefas->Categorias->find('list', ['limit' => 200]);
        $this->set(compact('tarefa', 'categorias'));
        $this->set('_serialize', ['tarefa']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tarefa id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tarefa = $this->Tarefas->get($id);
        if ($this->Tarefas->delete($tarefa)) {
            $this->Flash->success(__('A {0} foi excluida', 'Tarefa'));
        } else {
            $this->Flash->error(__('A {0} não pode ser excluida', 'Tarefa'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function concluirTarefa($id = null){
        $this->request->allowMethod(['post','put']);
        $tarefa = $this->Tarefas->get($id);
        $tarefa->concluido = true;
        if ($this->Tarefas->save($tarefa)) {
            $this->Flash->success(__('{0} Finalizada !', 'Tarefa'));

        } else {
            $this->Flash->error(__('A {0} não pode ser finalizada!', 'Tarefa'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
