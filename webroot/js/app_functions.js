function clear_all(document){
    $(document).find('input').each(function () {
        $(this).val('')
    });
    $(document).find('select').each(function () {
        $(this).val('')
        $(this).trigger('change');
    });
    $(document).find('input[type=radio]').each(function () {
        $(this).prop('checked',  false)
    });
    $(document).find('input[type=checkbox]').each(function () {
        $(this).prop('checked',  false)
    });

}

$(document).ready(function(){
    $("#btn-refresh").click(function () {
        var fr = $(this).parent().parent()[0];
        clear_all(fr);
    });
});